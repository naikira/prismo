class Redcarpet::Render::AccountBio < Redcarpet::Render::Base
  def paragraph(text)
    "<p>#{text}</p>"
  end

  def header(text, header_level)
    '<p>' + '#' * header_level + text + '</p>'
  end

  def block_html(raw_html)
    raw_html
  end
end
