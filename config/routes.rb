require 'sidekiq/web'

Rails.application.routes.draw do
  authenticate :user, lambda { |u| u.is_admin? } do
    mount Sidekiq::Web, at: 'admin/sidekiq', as: :sidekiq
  end

  devise_for :users, path: 'auth', controllers: {
    # omniauth_callbacks: 'auth/omniauth_callbacks',
    # sessions:           'auth/sessions',
    registrations:      'auth/registrations',
    # passwords:          'auth/passwords',
    # confirmations:      'auth/confirmations',
  }

  root to: 'stories#index'

  get '.well-known/host-meta', to: 'well_known/host_meta#show', as: :host_meta, defaults: { format: 'xml' }
  get '.well-known/webfinger', to: 'well_known/webfinger#show', as: :webfinger
  get '.well-known/x-nodeinfo2', to: 'well_known/nodeinfo2#show', as: :nodeinfo2
  get 'manifest', to: 'manifest#show', as: :manifest, defaults: { format: 'application/manifest+json' }

  get 'accounts/:id', to: 'accounts#show', as: :network_account

  get :design, to: 'static#design'

  resource :account, path: '@:username' do
    root to: 'accounts/stories#hot'
    get :recent, to: 'accounts/stories#recent', as: :recent_stories

    resources :comments, only: [] do
      root to: 'accounts/comments#hot'
      get :recent, on: :collection, controller: 'accounts/comments'
    end
  end

  resource :feed, defaults: { format: 'atom' } do
    get :hot, to: 'stories#index'
  end

  get '/stories/:id', to: redirect('/posts/%{id}')

  namespace :dashboard do
    resource :followed_accounts, only: [:show] do
      get :recent, on: :member
    end
  end

  resources :stories, path: :posts do
    get :recent, on: :collection
  end

  resources :tags, param: :name, only: [] do
    resources :stories, path: '/', controller: 'tags/stories', only: [] do
      get '/', action: :hot, on: :collection
      get :recent, on: :collection
    end
  end

  resources :comments, only: [:index, :show] do
    get :recent, on: :collection
  end

  resources :notifications, only: [:index] do
    get :unread, on: :collection
  end

  get '/ap/accounts/:username', to: redirect('/@%{username}')
  get '/ap/stories/:id', to: redirect('/stories/%{id}')
  get '/ap/comments/:id', to: redirect('/comments/%{id}')

  namespace :activitypub, path: :ap do
    resources :accounts, only: [], param: :username do
      get :outbox, on: :member

      resource :inbox, only: [:create], controller: 'account/inbox'
    end
  end

  namespace :api do
    # Salmon
    post '/salmon/:id', to: 'salmon#update', as: :salmon

    namespace :ujs do
      namespace :markdown do
        get :preview
      end

      resources :stories, only: [] do
        post :toggle_like, on: :member
        post :scrap, on: :member
        delete :destroy, on: :member, as: :delete
      end

      resources :comments, only: [:new, :create, :edit, :update, :destroy] do
        post :toggle_like, on: :member
      end

      resources :accounts, only: [] do
        post :toggle_follow, on: :member
      end
    end

    namespace :v1 do
      post 'tools/scrap_url', controller: :tools, action: :scrap_url

      resources :notifications, only: [] do
        get :unread_count, on: :collection
      end

      resources :tags, only: [:index]
    end
  end

  namespace :settings do
    resource :profile, only: [:show, :update]
  end

  namespace :admin do
    resource :settings, only: [:edit, :update]
    resources :accounts, except: [:delete] do
      post :suspend, on: :member
      post :silence, on: :member
      post :unsilence, on: :member
    end
    resources :domain_blocks, except: [:edit, :update]
  end
end
