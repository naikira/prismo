require 'rails_helper'

describe StoryPresenter do
  let(:actor) { create(:user, :with_account) }
  let(:story) { create(:story) }
  let(:options) { {} }

  let(:instance) { described_class.new(story, options) }

  describe '#more_actions?' do
    context 'when current_user is present' do
      let(:options) { { current_user: actor } }

      it 'does not rise exception' do
        expect { instance.more_actions? }.to_not raise_exception
      end
    end
  end
end
