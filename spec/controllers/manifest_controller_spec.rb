require 'rails_helper'

describe ManifestController, type: :controller do
  render_views

  describe 'GET #show' do
    before do
      get :show, format: :json
    end

    it 'returns successfull response' do
      expect(response).to be_successful
    end
  end
end
