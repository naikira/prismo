require 'rails_helper'

describe CommentsController, type: :controller do
  render_views

  let(:comment) { create(:comment) }

  describe 'GET #index' do
    subject { get :index }

    it 'renders successfull response' do
      subject
      expect(response).to be_success
    end
  end

  describe 'GET #recent' do
    subject { get :recent }

    it 'renders successfull response' do
      subject
      expect(response).to be_success
    end
  end

  describe 'GET #show' do
    subject { get :show, params: { id: comment.id } }

    it 'renders successfull response' do
      subject
      expect(response).to be_success
    end
  end
end
