require 'rails_helper'

describe ActivityPub::ProcessCollection, type: :service do
  let(:actor) { create(:account, domain: 'example.com', uri: 'http://example.com/account') }

  let(:payload) do
    {
      '@context': 'https://www.w3.org/ns/activitystreams',
      id: 'foo',
      type: 'Create',
      actor: ActivityPub::TagManager.instance.uri_for(actor),
      object: {
        id: 'bar',
        type: 'Article',
        content: 'Lorem ipsum',
      }
    }
  end

  let(:json) { Oj.dump(payload) }

  describe '#run' do
    subject { described_class.run(body: json, account: forwarder) }

    context 'when actor differs from sender' do
      let(:forwarder) { create(:account, domain: 'example.com', uri: 'http://example.com/other_account') }

      it 'processes payload with sender if no signature exists' do
        expect_any_instance_of(ActivityPub::LinkedDataSignature).not_to receive(:verify_account!)
        expect(ActivityPub::Activity).to receive(:factory).with(instance_of(Hash), forwarder, instance_of(Hash))

        subject
      end

      it 'processes payload with actor if valid signature exists' do
        payload['signature'] = {'type' => 'RsaSignature2017'}

        expect_any_instance_of(ActivityPub::LinkedDataSignature).to receive(:verify_account!).and_return(actor)
        expect(ActivityPub::Activity).to receive(:factory).with(instance_of(Hash), actor, instance_of(Hash))

        subject
      end

      it 'does not process payload if invalid signature exists' do
        payload['signature'] = {'type' => 'RsaSignature2017'}

        expect_any_instance_of(ActivityPub::LinkedDataSignature).to receive(:verify_account!).and_return(nil)
        expect(ActivityPub::Activity).to_not receive(:factory)

        subject
      end
    end

    context 'when account is suspended' do
      let(:forwarder) do
        create(:account, domain: 'example.com', uri: 'http://example.com/other_account', suspended: true)
      end

      it { expect(subject.result).to eq nil }

      it 'does not process collection' do
        expect_any_instance_of(ActivityPub::LinkedDataSignature).to_not receive(:verify_account!)
        expect(ActivityPub::Activity).to_not receive(:factory)

        subject
      end
    end
  end
end
