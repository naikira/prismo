require_relative '../../sections/story_row_section'
require_relative '../../sections/tag_heading_section'

class Tags::ShowPage < SitePrism::Page
  set_url '/tags{/name}'
  set_url_matcher %r{\/tags\/\w+\z}

  section :heading, TagHeadingSection, '.tag-head'
  sections :stories, StoryRowSection, '.stories-list li.story-row'
end
