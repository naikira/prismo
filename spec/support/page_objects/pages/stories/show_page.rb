require_relative '../../sections/navbar_section'
require_relative '../../sections/comments_tree_section'
require_relative '../../sections/story_row_section'

class Stories::ShowPage < SitePrism::Page
  set_url '/posts{/id}'
  set_url_matcher %r{\/posts\/\d+\z}

  element :vote_btn, '.story-row-score-btn'

  section :navbar, NavbarSection, '.navbar-main'
  section :comments_tree, CommentsTreeSection, '[data-controller="comments-tree"]'
  sections :stories, StoryRowSection, '.stories-list li.story-row'

  section :new_comment_form, '#new_comment' do
    element :comment_body_input, '#comment_body'
  end

  def toggle_vote
    vote_btn.click
  end
end
