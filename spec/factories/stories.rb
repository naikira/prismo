require 'faker'

FactoryBot.define do
  factory :story do
    account
    tag_names ['foo', 'bar']
    local true
    group

    trait :link do
      sequence(:url) { Faker::Internet.url }
      sequence(:title) { Faker::Company.catch_phrase }
    end

    trait :text do
      url nil
      sequence(:description) { Faker::Markdown.sandwich }
    end

    trait :not_local do
      local false
      uri Faker::Internet.device_token
    end

    trait :removed do
      removed { true }
    end
  end
end
