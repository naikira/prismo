require 'rails_helper'

feature 'Suspending account' do
  let(:home_page) { Stories::ShowPage.new }
  let(:sign_in_page) { SignInPage.new }

  let(:user) { create(:user, :with_account, password: 'TestPass') }

  before do
    user.account.update(suspended: true)
  end

  def sign_user_in
    sign_in_page.load
    sign_in_page.sign_in_using(user.email, 'TestPass')
  end

  scenario 'suspended user tries to sign-in' do
    sign_user_in
    expect(page).to have_content 'Your account has been suspended'
  end
end
