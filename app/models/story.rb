class Story < ApplicationRecord
  Gutentag::ActiveRecord.call self

  HOT_DAYS_LIMIT = 40

  attr_accessor :tag_list

  belongs_to :account, counter_cache: true, optional: true
  has_many :comments, dependent: :destroy
  belongs_to :url_meta, optional: true
  has_many :likes, as: :likeable
  belongs_to :group, optional: true

  validates :url, allow_blank: true, uniqueness: true

  delegate :thumb, :thumb_url, :thumb_data?,
           to: :url_meta,
           allow_nil: true

  scope :local, ->{ where(domain: nil) }

  def article?
    !url.present?
  end

  def object_type
    article? ? :article : :page
  end

  def self.find_local(id)
    find_by(id: id, domain: nil)
  end
end
