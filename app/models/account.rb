class Account < ApplicationRecord
  USERNAME_RE = /[a-z0-9_]+([a-z0-9_\.]+[a-z0-9_]+)?/i
  MENTION_RE  = /(?<=^|[^\/[:word:]])@((#{USERNAME_RE})(?:@[a-z0-9\.\-]+[a-z0-9]+)?)/i

  include AccountFinderConcern
  include AccountAvatarUploader[:avatar]

  has_one :user, inverse_of: :account
  has_many :likes
  has_many :stories
  has_many :comments
  has_many :notifications_as_recipient, class_name: :Notification, as: :recipient
  has_many :follow_requests, dependent: :destroy
  has_many :active_follows, class_name: 'Follow', foreign_key: 'account_id', dependent: :destroy
  has_many :passive_follows, class_name: 'Follow', foreign_key: 'target_account_id', dependent: :destroy
  has_many :followers, -> { order('follows.id desc') }, through: :passive_follows, source: :account
  has_many :following, -> { order('follows.id desc') }, through: :active_follows, source: :account

  validates :username, presence: true

  # Remote user validations
  validates :username, uniqueness: { scope: :domain, case_sensitive: true }, if: -> { !local? }

  # Local user validations
  validates :username, format: { with: /\A[a-z0-9_]+\z/i },
                       length: { maximum: 30 }, if: -> { local? },
                       uniqueness: { scope: :domain, case_sensitive: true }
  validates :display_name, length: { maximum: 30 }, if: -> { local? }

  scope :remote, -> { where.not(domain: nil) }
  scope :local, -> { where(domain: nil) }
  scope :expiring, ->(time) { remote.where.not(subscription_expires_at: nil).where('subscription_expires_at < ?', time) }
  scope :matches_username, ->(value) { where(arel_table[:username].matches("#{value}%")) }
  scope :matches_display_name, ->(value) { where(arel_table[:display_name].matches("#{value}%")) }
  scope :matches_domain, ->(value) { where(arel_table[:domain].matches("%#{value}%")) }

  scope :active_halfyear, ->{ where('last_active_at > ?', Time.current - 1.year) }
  scope :active_month, ->{ where('last_active_at > ?', Time.current - 1.month) }

  delegate :is_admin, to: :user, allow_nil: true

  before_create :generate_keys

  def self.inboxes
    reorder(nil).pluck(Arel.sql("distinct coalesce(nullif(accounts.shared_inbox_url, ''), accounts.inbox_url)"))
  end

  def liked_comments
    likes.where(likeable_type: 'Comment')
  end

  def liked_stories
    likes.where(likeable_type: 'Story')
  end

  def liked?(likeable)
    likes.where(likeable: likeable).any?
  end

  def local?
    domain.nil?
  end

  def remote?
    !local?
  end

  def bot?
    %w(Application Service).include? actor_type
  end

  def acct
    local? ? username : "#{username}@#{domain}"
  end

  def local_username_and_domain
    "#{username}@#{Rails.configuration.x.local_domain}"
  end

  def to_webfinger_s
    "acct:#{local_username_and_domain}"
  end

  def subscribed?
    subscription_expires_at.present?
  end

  def possibly_stale?
    last_webfingered_at.nil? || last_webfingered_at <= 1.day.ago
  end

  def refresh!
    return self if local?
    ResolveAccountService.new.call(acct)
  end

  def keypair
    @keypair ||= OpenSSL::PKey::RSA.new(private_key || public_key)
  end

  def magic_key
    modulus, exponent = [keypair.public_key.n, keypair.public_key.e].map do |component|
      result = []

      until component.zero?
        result << [component % 256].pack('C')
        component >>= 8
      end

      result.reverse.join
    end

    (['RSA'] + [modulus, exponent].map { |n| Base64.urlsafe_encode64(n) }).join('.')
  end

  def object_type
    :person
  end

  def follow!(other_account, uri: nil)
    active_follows.create_with(uri: uri)
                  .find_or_create_by!(target_account: other_account)
  end

  def unfollow!(other_account)
    follow = active_follows.find_by(target_account: other_account)
    follow&.destroy
  end

  def requested_follow?(other_account)
    follow_requests.where(target_account: other_account).exists?
  end

  def following?(other_account)
    active_follows.where(target_account: other_account).exists?
  end

  def refresh_karma
    update(
      comments_karma: comments.sum(:likes_count),
      posts_karma: stories.sum(:likes_count)
    )
  end

  private

  def generate_keys
    return unless local? && !Rails.env.test?

    keypair = OpenSSL::PKey::RSA.new(2048)
    self.private_key = keypair.to_pem
    self.public_key  = keypair.public_key.to_pem
  end
end
