module ActivityPub
  class DomainBlock < ApplicationRecord
    SEVERITIES = %i[silence suspend].freeze

    enum severity: SEVERITIES

    validates :domain, presence: true, uniqueness: true
    validates :severity, presence: true

    has_many :accounts
  end
end
