class Stories::Delete < ActiveInteraction::Base
  object :story

  def execute
    return false if story.removed?
    @account = story.account

    story.update!(
      account: nil,
      title: nil,
      description: nil,
      uri: nil,
      url: nil,
      url_meta: nil,
      url_domain: nil,
      domain: nil,
      removed: true
    )

    distribute_delete_story! if story.local?

    story
  end

  private

  attr_reader :account

  def distribute_delete_story!
    delivery_inboxes.each do |inbox_url|
      ActivityPub::DeliveryJob.perform_later(
        signed_payload, account.id, inbox_url
      )
    end
  end

  def signed_payload
    @signed_payload ||= begin
      Oj.dump(
        ActivityPub::LinkedDataSignature.new(payload).sign!(account)
      )
    end
  end

  def payload
    ActivityPub::DeleteSerializer.new(story, with_context: true).as_json
  end

  def delivery_inboxes
    Account.inboxes.reject(&:empty?)
  end
end
