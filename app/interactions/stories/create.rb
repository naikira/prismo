require 'active_interaction'

class Stories::Create < Stories::CreateUpdateBase
  validates :url, url: { allow_blank: true }
  validate :url_or_description_required

  def execute
    story = Story.new(inputs)
    story.account = account

    story.url_domain = URI.parse(url).host if url.present?
    story.group = Group.find_by!(supergroup: true)
    story.tag_names = tags
    story.local = true

    if story.save
      after_story_save_hook(story)
    else
      errors.merge!(story.errors)
    end

    story
  end

  private

  def after_story_save_hook(story)
    account.touch(:last_active_at)

    Stories::Like.run!(story: story, account: account)
    Stories::ScrapJob.perform_later(story.id)
    ActivityPub::DistributionJob.call(story)
  end
end
