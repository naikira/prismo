class CommentPolicy < ApplicationPolicy
  def index?
    true
  end

  def create?
    user.present? && !user.account.silenced? && !user.account.suspended?
  end

  def update?
    user.present? && (user.is_admin? || user == record.account.user)
  end

  def comment?
    user.present? && !user.account.silenced? && !user.account.suspended?
  end

  def toggle_like?
    user.present? && !user.account.silenced? && !user.account.suspended?
  end

  def destroy?
    user.present? && (user.is_admin? || user == record.account.user)
  end
end
