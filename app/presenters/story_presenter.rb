class StoryPresenter
  include ActionView::Helpers::UrlHelper
  include ActionView::Helpers::TextHelper

  attr_reader :story, :liked_ids, :current_user, :account

  def initialize(record, options = {})
    @story = record.removed? ? RemovedStoryNull.new(record) : record
    @liked_ids = options[:liked_ids] || []
    @current_user = options[:current_user]
    @account = story.account.decorate
  end

  def excerpt
    if story.url_meta&.description
      story.url_meta.description.truncate(100)
    else
      story.decorate.description_excerpt
    end
  end

  def tags
    story.tags.to_a.map(&:decorate)
  end

  def primary_tags
    tags.shift(2)
  end

  def more_tags
    more_tags_seleciton = tags - primary_tags
    more_tags_seleciton.any? ? more_tags_seleciton : []
  end

  def thumb_classes
    classes = []
    return if story.thumb.present?

    classes.push('story-row-thumb-empty')
    classes.push('story-row-thumb-empty--article') if story.article?
    classes.push('story-row-thumb-empty--removed') if story.removed?

    classes
  end

  def root_classes
    classes = []

    classes.push('story-row--liked') if liked?
    classes.push('story-row--removed') if story.removed?

    classes
  end

  def thumb_styles
    return nil if story.thumb.blank?

    "background-image: url(#{story.thumb_url(:size_200)})"
  end

  def liked?
    liked_ids.include? story.id
  end

  def more_actions?
    current_user.present? && (policy.scrap? || policy.edit? || policy.destroy?)
  end

  private

  def policy
    @policy ||= StoryPolicy.new(current_user, story)
  end
end
