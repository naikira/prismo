class CommentPresenter
  include ActionView::Helpers::UrlHelper
  include ActionView::Helpers::TextHelper
  include ActionView::Helpers::TagHelper

  attr_reader :comment, :account, :options, :liked_ids, :current_user

  def initialize(comment, options = {})
    @comment = comment.removed? ? RemovedCommentNull.new(comment) : comment
    @account = @comment.account.decorate
    @liked_ids = options[:liked_ids]
    @current_user = options[:current_user]
    @options = options
  end

  def linked_account_username
    if account.id.present?
      link_to(account.username_with_at, account.path, class: 'p-author')
    else
      account.username_with_at
    end
  end

  def like_button_label
    pluralize(comment.likes_count, 'vote')
  end

  def show_parent_link?
    options.key?(:show_parent_link) && show_parent_link && comment.parent_id ? true : false
  end

  def show_story?
    options.key?(:show_story) && options[:show_story] ? true : false
  end

  def show_children?
    options.key?(:show_children) && options[:show_children] ? true : false
  end

  def show_reply_link?
    return false unless CommentPolicy.new(current_user, comment).comment?

    options.key?(:show_reply_link) ? options[:show_reply_link] : true
  end

  def show_edit?
    !comment.removed? && CommentPolicy.new(current_user, comment).edit?
  end

  def show_delete?
    !comment.removed? && CommentPolicy.new(current_user, comment).destroy?
  end

  def can_like?
    !comment.removed? && current_user.present? && !current_user.silenced?
  end

  def liked?
    liked_ids.include? comment.id
  end

  def has_child_comments?
    comment.children_count.positive?
  end

  def root_classes
    classes = []

    classes.push('comment--liked') if liked?
    classes.push('comment--removed') if comment.removed?
    classes.push('comment--silenced') if comment.account.silenced?

    classes
  end

  def body_html
    if comment.account.silenced?
      content_tag :p, 'Account has been silenced'
    else
      comment.body_html
    end
  end
end
