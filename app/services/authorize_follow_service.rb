class AuthorizeFollowService < BaseService
  def call(source_account, target_account, **options)
    if options[:skip_follow_request]
      follow_request = FollowRequest.new(account: source_account,
                                         target_account: target_account,
                                         uri: options[:follow_request_uri])
    else
      follow_request = FollowRequest.find_by!(account: source_account,
                                              target_account: target_account)
      follow_request.authorize!
    end

    create_notification(follow_request) if source_account.remote?

    follow_request
  end

  private

  def create_notification(follow_request)
    ActivityPub::DeliveryJob.perform_later(signed_payload(follow_request),
                                           follow_request.target_account_id,
                                           follow_request.account.inbox_url)
  end

  def signed_payload(follow_request)
    Oj.dump(
      ActivityPub::LinkedDataSignature.new(payload(follow_request))
        .sign!(follow_request.target_account)
    )
  end

  def payload(follow_request)
    ActivityPub::AcceptFollowSerializer.new(follow_request,
                                            with_context: true).as_json
  end
end
