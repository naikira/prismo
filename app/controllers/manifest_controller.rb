class ManifestController < ActionController::Base
    def show
      render json: ManifestSerializer.new
    end
  end
  