class ActivityPub::Account::InboxController < ActivityPub::BaseController
  include SignatureVerification

  def create
    if signed_request_account
      process_payload
      head 202
    else
      render plain: signature_verification_failure_reason, status: 401
    end
  end

  private

  def set_account
    @account = Account.find_local!(params[:account_username]) if params[:account_username]
  end

  def body
    @body ||= request.body.read
  end

  def process_payload
    ActivityPub::ProcessingJob.perform_later(signed_request_account.id, body.force_encoding('UTF-8'))
  end
end
