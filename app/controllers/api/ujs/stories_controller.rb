class Api::Ujs::StoriesController < Api::Ujs::BaseController
  def toggle_like
    user_needed
    story = find_story
    authorize story

    outcome = Stories::ToggleLike.run(story: story, account: current_user.account)
    set_account_liked_story_ids

    render 'stories/_story', layout: false, locals: { story: outcome.result.likeable }
  end

  def scrap
    story = find_story
    authorize story

    Stories::Rescrap.run(story: story, user: current_user)

    head :no_content
  end

  def destroy
    story = find_story
    authorize story

    Stories::DeleteJob.perform_later(story.id)

    head :ok
  end

  private

  def find_story
    Story.find(params[:id])
  end
end
