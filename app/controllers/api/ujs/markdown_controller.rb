class Api::Ujs::MarkdownController < Api::Ujs::BaseController
  def preview
    html = BodyParser.new(params[:body]).call

    render plain: html
  end
end
