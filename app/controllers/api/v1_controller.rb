class Api::V1Controller < ApiController
  private

  def set_pagination_headers(col)
    response.set_header('X-Pagination-Total-Count', col.total_count)
    response.set_header('X-Pagination-Total-Pages', col.total_pages)
    response.set_header('X-Pagination-Page', col.current_page)
    response.set_header('X-Pagination-Next-Page', col.next_page)
    response.set_header('X-Pagination-Prev-Page', col.prev_page)
  end
end
