module Accounts
  class SuspendJob < ApplicationJob
    queue_as :default

    def perform(account_id, remove_user = false)
      SuspendAccountService.new.call(
        Account.find(account_id),
        including_user: remove_user
      )
    end
  end
end
