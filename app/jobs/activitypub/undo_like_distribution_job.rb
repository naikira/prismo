class ActivityPub::UndoLikeDistributionJob < ApplicationJob
  queue_as :push

  def self.call(like)
    ActivityPub::UndoLikeDistributionJob.perform_later(
      like.id,
      like.likeable_type,
      like.likeable_id,
      like.account_id
    )
  end

  def perform(like_id, likeable_type, likeable_id, account_id)
    @like_id = like_id
    @likeable = likeable_type.constantize.find(likeable_id)
    @account = Account.find(account_id)

    inboxes.each do |inbox_url|
      ActivityPub::DeliveryJob.perform_later(
        signed_payload, account.id, inbox_url
      )
    end

  rescue ActiveRecord::RecordNotFound
    true
  end

  private

  attr_reader :likeable, :account

  def inboxes
    if likeable.is_a?(Comment)
      [likeable.account.inbox_url]
    else
      []
    end
  end

  def signed_payload
    @signed_payload ||= Oj.dump(ActivityPub::LinkedDataSignature.new(payload).sign!(account))
  end

  def payload
    @payload ||= ActivityPub::UndoLikeSerializer.new(likeable, with_context: true).as_json
  end
end
