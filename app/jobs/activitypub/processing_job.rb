class ActivityPub::ProcessingJob < ApplicationJob
  queue_as :default

  def perform(account_id, body)
    ActivityPub::ProcessCollection.run!(
      body: body,
      account: ::Account.find(account_id),
      override_timestamps: true
    )
  end
end
