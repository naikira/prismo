class Stories::DeleteJob < ApplicationJob
  queue_as :default

  def perform(story_id)
    Stories::Delete.run!(story: Story.find(story_id))
  end
end
