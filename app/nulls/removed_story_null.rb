class RemovedStoryNull
  attr_accessor :story

  def initialize(story = Story.new)
    @story = story.decorate
  end

  def title
    'Story removed'
  end

  def description
    'This story has been removed'
  end

  def account
    Account.new
  end

  def decorate
    StoryDecorator.new(self)
  end

  delegate :id,
           :likes_count,
           :created_at,
           :to_model,
           :removed?,
           :thumb,
           :article?,
           :url?,
           :tags,
           :url_meta,
           to: :story

  class Account
    def id
      nil
    end

    def avatar_url(size = :size_60)
      '/placeholders/avatar.jpg'
    end

    def path
      nil
    end

    def decorate
      self
    end

    def username
      'Ghost'
    end

    def username_with_at
      '@' << username
    end

    def silenced?
      false
    end

    def suspended?
      false
    end
  end
end
