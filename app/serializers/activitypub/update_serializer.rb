class ActivityPub::UpdateSerializer < ActivityPub::BaseSerializer
  def data
    {
      id: id,
      type: 'Update',
      object: object_serialized,
      actor: actor
    }
  end

  def id
    [ActivityPub::TagManager.instance.uri_for(object), '#updates/', object.updated_at.to_i].join
  end

  def actor
    resource = object.is_a?(Account) ? object : object.account
    ActivityPub::TagManager.instance.uri_for(resource)
  end

  def object_serialized
    case object
    when Account
      ActivityPub::ActorSerializer.new(object).data
    when Story
      ActivityPub::StorySerializer.new(object).data
    when Comment
      ActivityPub::CommentSerializer.new(object).data
    end
  end
end
