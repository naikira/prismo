class ActivityPub::ActivitySerializer < ActivityPub::BaseSerializer
  def data
    {
      id: ActivityPub::TagManager.instance.activity_uri_for(object),
      object: parent_object,
      type: 'Create',
      actor: ActivityPub::TagManager.instance.uri_for(object.account),
      published: object.created_at.iso8601,
      to: ActivityPub::TagManager.instance.to(object),
      cc: ActivityPub::TagManager.instance.cc(object)
    }
  end

  private

  def parent_object
    case object
    when Story then ActivityPub::StorySerializer.new(object).data
    when Comment then ActivityPub::CommentSerializer.new(object).data
    end
  end
end
