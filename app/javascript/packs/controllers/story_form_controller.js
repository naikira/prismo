import BaseController from './base_controller'
import axios from 'axios'
import Tagify from '@yaireo/tagify'
import api from '../lib/api'
import { debounce } from "debounce"

export default class extends BaseController {
  static targets = ['urlInput', 'titleInput', 'fetchTitleBtn', 'tagsInput', 'tagsPhantomInput']

  connect () {
    this._initTags()
  }

  fetchTitle (e) {
    e.preventDefault()
    e.target.classList.add('loading')

    let req = axios.post('/api/v1/tools/scrap_url', {
      url: this.url
    })

    req.then((resp) => {
      this.title = resp.data.title
      this.url = resp.data.url
      e.target.classList.remove('loading')
    })

    req.catch(() => {
      e.target.classList.remove('loading')
    })
  }

  handleUrlChange () {
    this.urlDisabled = this.urlInputTarget.value.length == 0
  }

  _initTags () {
    let _this = this

    this.tagify = new Tagify(this.tagsPhantomInputTarget, {
      delimiters: ', ',
      maxTags: this.tagsPhantomInputTarget.dataset.maxTags,

      // This `foo` is here because of a bug, see:
      // https://github.com/yairEO/tagify/issues/122
      whitelist: ['foo'],

      transformTag (value) { return value.replace('#', '') }
    })

    this.tagify.on('add', () => { this._updateTagsInput() })
    this.tagify.on('remove', () => { this._updateTagsInput() })

    let fetchTags = debounce((e) => { this._fetchTags(e) }, 300)
    this.tagify.on('input', (e) => fetchTags(e))
  }

  _updateTagsInput () {
    let val = this.tagify.value.map((tag) => { return tag.value }).join(',')
    this.tagsList = val
  }

  _fetchTags (e) {
    let value = e.detail.value

    this.tagify.settings.whitelist.length = 0
    let req = api.tags({ q: value })

    req.then((resp) => {
      this.tagify.settings.whitelist = resp.data.map((tag) => { return tag.name })
      this.tagify.dropdown.show.call(this.tagify, value)
    })
  }

  get url () {
    return this.urlInputTarget.value
  }

  set title (title) {
    this.titleInputTarget.value = title
  }

  set url (url) {
    this.urlInputTarget.value = url
  }

  set urlDisabled (disable) {
    disable ?
      this.fetchTitleBtnTarget.setAttribute('disabled', 'disabled') :
      this.fetchTitleBtnTarget.removeAttribute('disabled')
  }

  set tagsList (value) {
    this.tagsInputTarget.setAttribute('value', value)
  }
}
