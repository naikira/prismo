class CreateComments < ActiveRecord::Migration[5.2]
  def change
    create_table :comments do |t|
      t.references :story, foreign_key: true
      t.references :account, foreign_key: true
      t.text :body
      t.integer :parent_id, foreign_key: true
      t.integer :votes_count

      t.timestamps
    end
    add_index :comments, :parent_id
  end
end
