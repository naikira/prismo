class AddDomainToStory < ActiveRecord::Migration[5.2]
  def change
    add_column :stories, :domain, :string
    add_index :stories, :domain
  end
end
